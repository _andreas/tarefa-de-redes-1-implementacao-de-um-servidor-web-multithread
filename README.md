# README #

Tarefa 1 de programação da disciplina Redes de Computadores. Um servidor web multithread em Java, 
de acordo com o template providenciado.


### Como funciona? ###

Só rodar WebServer.java e, usando o endereço IP da máquina ou o seu nome (ou localhost se checando na própria máquina)
:8001, dá para acessar os arquivos e páginas HTML presentes. 
Por exemplo:
pc:8001/index.html

É importante que os arquivos e pastas relacionados estejam no mesmo diretório da pasta src, mas não DENTRO dela.


