import java.io.* ;
import java.net.* ;
import java.util.* ;

// colocar os arquivos (html, pastas, imagens) na mesma pasta em que está o source.
public final class WebServer
{
	public static void main(String argv[]) throws Exception
	{
		// Set the port number.
		int port = 8001;
		ServerSocket welcomeSocket = new ServerSocket(port);

         while(true)
         {
            Socket connectionSocket = welcomeSocket.accept();

			// Construct an object to process the HTTP request message.
			HttpRequest request = new HttpRequest(connectionSocket);

			// Create a new thread to process the request.
			Thread thread = new Thread(request);  

			// Start the thread.
			thread.start();
         }
	}
}