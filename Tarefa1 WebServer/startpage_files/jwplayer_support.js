/*------jwplayer_support.js------*/
var isFlashExists = '';
var isChrome = '';
var isFirefox = '';
var isMac = '';


$(document).ready(function(){
					if (typeof swfobject != "undefined") {
					  isFlashExists = swfobject.hasFlashPlayerVersion('10') ? true : false ;
					}
    isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    isMac     = navigator.userAgent.toLowerCase().indexOf('macintosh') > -1;

    if(isChrome) {
        isFlashExists = ((typeof navigator.plugins != "undefined" && typeof navigator.plugins["Shockwave Flash"] == "object") || (window.ActiveXObject && (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) != false));
	}
	var http_host = get_http_host();
    if(isFlashExists&&isFirefox) {
	  if(http_host.indexOf('https://startpage') > -1) {
		changeVideoLinks('startpage');
	  } else if (http_host.indexOf('https://ixquick') > -1) {
		changeVideoLinks('ixquick');
	  }
	}

});

var videoOffset = 0;
var topopupmarginval = 0;

function calcVerticalPosition () {
    var videoHeight = 0, videoWidth = 0, marginVal = 0, wrapOffset = 0, marginLeftVal = 0, borderWidth = 0, padLeft = 0;
    var winHeight = 'innerHeight' in window ? window.innerHeight : jQuery(window).height();
    var winWidth = 'innerWidth' in window ? window.innerWidth : jQuery(window).width();
    var closeButton = jQuery("<div class='close'></div>");
    // negation forces the value to be boolean
    var isFlashPlayerLoaded = !!jQuery('#sm-video_wrapper').length;
    var isHTMLPlayerLoaded = !!jQuery('#toPopup').find('div.jwplayer').length;
    var isTabletSafari = !!(MY_BROWSER === 'Mobile Safari' && DEVICE_TYPE === 'Tablet');
    var Html5PlayerloadedinDesktop = 0;
    if (isFlashPlayerLoaded) {
        videoHeight = jQuery('#sm-video_wrapper object').height();
        //videoWidth = jQuery('#sm-video_wrapper object').width();
        videoWidth = jQuery('#sm-video_wrapper').outerWidth();
        //borderWidth = jQuery('#sm-video_wrapper').css('borderWidth');
    } else if (isHTMLPlayerLoaded) {
        videoHeight = jQuery('#toPopup').find('div.jwplayer').height();
        videoWidth = jQuery('#sm-video').width();
        borderWidth = jQuery('#sm-video').css('borderWidth');
        jQuery('div.close').remove();
        if (!jQuery('#popup_content').find('div.close').length) {
            jQuery('#toPopup').find('div.jwplayer').prepend(closeButton);
        }
        //changes to restore dimention of popup after default player changes from
        //flash to html5 for desktops
        if( !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
            Html5PlayerloadedinDesktop = 1;
        }
    }
    // get borderWidth value
    borderWidth = (borderWidth == 0) ? borderWidth : parseInt(borderWidth.replace('px', ''), 10);
    videoHeight = videoHeight + (borderWidth * 2);
    videoWidth = videoWidth + (borderWidth * 2);
    marginVal = (winHeight - videoHeight) / 2;
    if (videoOffset === 0 && (isFlashPlayerLoaded || (isHTMLPlayerLoaded && isTabletSafari && winWidth > 768 && winWidth <= 1024) )) {
        wrapOffset = jQuery('#toPopup').offset().top;
        if (isTabletSafari) {
            videoOffset = jQuery('#toPopup').find('div.jwplayer').offset().top;
        } else {
            videoOffset = jQuery('#sm-video_wrapper').offset().top;
        }
        if ((videoOffset - wrapOffset) != 0) {
            videoOffset = videoOffset - wrapOffset;
        }
    }
    if (isTabletSafari && winWidth > 768) {
        marginVal = marginVal - videoOffset;
    }
    
    if (winWidth <= 768) {
        marginVal = isTabletSafari ? marginVal : (marginVal - videoOffset);
        // we also need to horizontally center the video if window width is less than 768
        if (isFlashPlayerLoaded || (isHTMLPlayerLoaded && isTabletSafari) || (Html5PlayerloadedinDesktop == 1)) {
            marginLeftVal = (winWidth - videoWidth) / 2;
            padLeft = jQuery('#toPopup').css('padding-left');
            padLeft = parseInt(padLeft.replace('px', ''), 10);
            jQuery('#toPopup').css('margin-left', marginLeftVal);                
        }
    } else {
        jQuery('#toPopup, #sm-video_wrapper, div.jwplayer').css('margin-left', '');        
    }
    if (isFlashPlayerLoaded) {
        jQuery('#toPopup').css({'margin-top': marginVal, 'visibility': 'visible'});
        jQuery('#sm-video_wrapper').css('visibility', 'visible');
    } else {
        if (Html5PlayerloadedinDesktop == 0 ) {
            jQuery('#toPopup').css({'margin-top': marginVal, 'visibility': 'visible', 'height': '0px'});
        }else {
            topopupmarginval = marginVal;
            jQuery('#toPopup').css({'margin-top': marginVal, 'visibility': 'visible'});
            var tOut;
            clearInterval(tOut);
            tOut = setTimeout(function () {
                var xy = jQuery('#toPopup').offset();
                jQuery('#tempstyle').remove();
		        var diff = parseInt(xy.top) - parseInt(topopupmarginval);
                jQuery('body').append('<style id=tempstyle>.tofix{top:'+ diff +'px!important;position:absolute!important}</style>');
                jQuery('#toPopup').addClass('tofix');
            },1000);
        }

        if (isTabletSafari ) {
            jQuery('#toPopup').find('div.jwplayer').css('left', '0%');
        }
    }
}

function changeVideoLinks(site) { 
  if (site == 'startpage') {
    jQuery('.topopup').each(function(){jQuery(this).attr('onclick',jQuery(this).attr('onclick').replace('https://startpage','https://www.startpage'))});
  } else if (site == 'ixquick') {
    jQuery('.topopup').each(function(){jQuery(this).attr('onclick',jQuery(this).attr('onclick').replace('https://ixquick','https://www.ixquick'))});
  }
}

function no_flash_alert(flash_alert_message) {
    alert(flash_alert_message);
}

jQuery(function($) {
    $("a.topopup").click(function() {
        if(isFlashExists) {
            loading(); // loading
            setTimeout(function(){ // then show popup, deley in .5 second
                loadPopup(); // function show popup 
            }, 500); // .5 second
        }
        return false;
    });

    /* For responsive design, we need to close the popup on clicking close button and also on the grey background */
    //$("div.close").click(function() {
    $("body").on("click", "div.close, #loadingId", function(e) {
    // For responsive design, we trigger click on loadingId on resize to handle responsiveness of popups
    // To close the JW player, we will only execute remove if the click is manually done by user
    var isPlayerPresent = $('#sm-video_wrapper').length || $('div.jwplayer').length;
    if ($(this).hasClass('close') || ($(this).attr('id') == 'loadingId' && typeof e.isTrigger === 'undefined' && isPlayerPresent)) {
        disablePopup(); 
        jwplayer("sm-video").stop();
        jwplayer("sm-video").remove();
        if (jQuery('#toPopup').hasClass('tofix')){
            jQuery('#toPopup').removeClass('tofix');
        }
        if( !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
            jQuery('#toPopup').hide();
        }
    }
    $('#loadingId').hide();
    if (typeof blockPageScrolling === 'function') {
      blockPageScrolling('loadingId');
    }
    });
	

    function loading() {
	  $("div.toPopup").fadeIn("normal");  
    }

    function closeloading() {
	$("div.toPopup").css('display','none');  
    }

    var popupStatus = 0; // set value
	
    function loadPopup() { 
	if(popupStatus == 0) { // if value is 0, show popup
	    closeloading(); // fadeout loading
	    $("#toPopup").fadeIn(0500); // fadein popup div
	    $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
	    $("#backgroundPopup").fadeIn(0001); 
	    popupStatus = 1; // and set value to 1
	}	
    }

    function disablePopup() {
	if(popupStatus == 1) { // if value is 1, close popup
	    $("#toPopup").fadeOut("normal");  
	    $("#backgroundPopup").fadeOut("normal");  
	    popupStatus = 0;  // and set value to 0
	    if (jwplayer().getState() != 'PAUSED') {
		jwplayer().pause(true);
	    }
	}
    }

});

var scrollPosition = 0;

function addVideo(event, video_file,flash_alert_message, player_override) {
  event.preventDefault ? event.preventDefault() : (event.returnValue = false);
  event.stopPropagation ? event.stopPropagation() : (event.returnValue = false);
    
  //Making html5 as default player for all mobile and desktop platforms
  //Default it to the attributes of the player_override object if it is there to use as jwplayer configuration
  //if (typeof player_override !== "undefined" || isFlashExists || navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) { // for the mobile devices that may not have flash we need to call jwplayer setup
    var primary_mode = 'html5';
    //if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)){
    //     primary_mode = 'html5';
    //}
    var autostart_val = true;
    if (typeof player_override !== "undefined") {
        primary_mode = player_override["player"];
        autostart_val = player_override["autostart"];
    }

    scrollPosition = $(document).scrollTop();
    if (typeof config_data != "undefined" && config_data['rsd'] == 1){
      jwplayer("sm-video").setup({
	    file: video_file,
            primary: primary_mode,
            width: "100%",
            aspectratio: "20:11",
            autostart: autostart_val,
            stretching: 'fill',
            wmode: "transparent",
            events: {
              onPlay: function(event){
              jwplayer().setControls(true);
            }
          },
            analytics: {
              cookies: false,
              enabled: false
              }
		});
    }else{
      jwplayer("sm-video").setup({
	    file: video_file,
            primary: primary_mode,
            width: 640,
            height: 360,
            autostart: true,
            stretching: 'fill',
            wmode: "transparent",
            events: {
              onPlay: function(event){
              jwplayer().setControls(true);
            }
          },
            analytics: {
              cookies: false,
              enabled: false
              }
		});

    }

    jQuery('#toPopup').show();
    jQuery("div.close").delegate(this,'click',function(){
									 jQuery('#toPopup').hide();
								   });
    //} else {
	//	no_flash_alert(flash_alert_message);
    //}
    if ($("html").hasClass("is-not-touch-device")) {
	jwplayer("sm-video").onFullscreen(function(e) {
		jQuery('html').removeClass('fullfixed');
		if (e.fullscreen === true) {
			jQuery('html').addClass('fullfixed');	
		} else {
			if (jQuery('#toPopup').hasClass('tofix')){
	                    jQuery('#toPopup').removeClass('tofix');
                    	    $("#tempstyle").remove();
                	}
			jQuery('html').removeClass('fullfixed');
                	jQuery('html,body').animate({
                    		scrollTop: scrollPosition
                	}, {
                    		duration: 0,
                    		complete: function () {
                        		calcVerticalPosition("");
                   		    }
                	});
		    }
	    });
    }
}

function get_http_host () {                                                                                                                                                                                  
  var http_host = '';                                                                                                                                                                                      
  var page_url = document.URL;                                                                                                                                                                             
  var url_parts = page_url.split('://');                                                                                                                                                                   
  var protocol = url_parts[0];                                                                                                                                                                             
                                                                                                                                                                                                             
  if (url_parts[1].indexOf('/') != -1) {                                                                                                                                                                   
	var slash_pos = url_parts[1].indexOf('/');                                                                                                                                                           
	http_host = protocol + '://' + url_parts[1].substr(0, slash_pos);                                                                                                                                    
  } else {                                                                                                                                                                                                 
	http_host = protocol + '://' + url_parts[1];                                                                                                                                                         
  }                                                                                                                                                                                                        
                                                                                                                                                                                                             
  return http_host;                                                                                                                                                                                        
}                   
/* add key for jwplayer */
jwplayer.key='9OTAGe6swAaUb3jUw7G+MUBBtPKR6rOHmrbjraHb0Og=';

/*------end------*/
